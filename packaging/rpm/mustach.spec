#---------------------------------------------
# spec file for package mustach
#---------------------------------------------

Name:           mustach
Version:        1.2.5
%if 0%{?fedora} >= 1
Release:        %{autorelease}
%else
Release:        1
%endif
License:        ISC
Summary:        Tiny Mustach processor
Url:            https://gitlab.com/jobol/mustach

%define         _disable_source_fetch 0
Source0:         %{url}/-/archive/%{version}/%{name}-%{version}.tar.bz2
%global         spec_tag 1.3.0
Source1:        https://github.com/mustache/spec/archive/refs/tags/v%{spec_tag}.tar.gz

%global so_version 1

BuildRequires:  make
BuildRequires:  gcc
BuildRequires:  valgrind
BuildRequires:  pkgconfig(json-c)
BuildRequires:  pkgconfig(libcjson)
BuildRequires:  pkgconfig(jansson)

%description
Tiny tool for processing JSON files with Mustache templates.

#---------------------------------------------
%package lib-core

Summary:        Core library of mustach

%description lib-core
Core library of mustach

#---------------------------------------------
%package lib-core-devel

Summary:        Core library of mustach - Development files
Requires:       %{name}-lib-core%{?_isa} = %{version}-%{release}

%description lib-core-devel
Core development files for mustach core library


#---------------------------------------------
%package lib-json-c

Summary:        The json-c library of mustach

%description lib-json-c
Mustach library for using json-c library

#---------------------------------------------
%package lib-json-c-devel

Summary:        The json-c library of mustach - Development files
Requires:       %{name}-lib-json-c%{?_isa} = %{version}-%{release}
Requires:       %{name}-lib-core-devel%{?_isa} = %{version}-%{release}

%description lib-json-c-devel
Development files for mustach library using json-c library


#---------------------------------------------
%package lib-cjson

Summary:        The cjson library of mustach

%description lib-cjson
Mustach library for using cjson library

#---------------------------------------------
%package lib-cjson-devel

Summary:        The cjson library of mustach - Development files
Requires:       %{name}-lib-cjson%{?_isa} = %{version}-%{release}
Requires:       %{name}-lib-core-devel%{?_isa} = %{version}-%{release}

%description lib-cjson-devel
Development files for mustach library using cjson library


#---------------------------------------------
%package lib-jansson

Summary:        The jansson library of mustach

%description lib-jansson
Mustach library for using jansson library

#---------------------------------------------
%package lib-jansson-devel

Summary:        The jansson library of mustach - Development files
Requires:       %{name}-lib-jansson%{?_isa} = %{version}-%{release}
Requires:       %{name}-lib-core-devel%{?_isa} = %{version}-%{release}

%description lib-jansson-devel
Development files for mustach library using jansson library


#---------------------------------------------
%global pkgdir %{_libdir}/pkgconfig

#---------------------------------------------
%prep
%autosetup
%setup -q -T -D -b 1
mv ../spec-%{spec_tag} test-specs/spec
sed -r -i 's/\bgit\b/:/' Makefile

#---------------------------------------------
%global mustach_conf tool=jsonc libs=split jsonc=yes cjson=yes jansson=yes

%check
%make_build test %{mustach_conf}

%build
%set_build_flags
%make_build %{mustach_conf}

%install
%make_install %{mustach_conf} \
    PREFIX=%{_prefix} \
    BINDIR=%{_bindir} \
    LIBDIR=%{_libdir} \
    INCLUDEDIR=%{_includedir} \
    MANDIR=%{_mandir} \
    PKGDIR=%{pkgdir}

#---------------------------------------------
%files
%{_bindir}/mustach
%{_mandir}/man1/mustach.1*
%license LICENSE.txt

#---------------------------------------------
%files lib-core
%{_libdir}/libmustach-core.so.%{so_version}{,.*}
%license LICENSE.txt

#---------------------------------------------
%files lib-core-devel
%{_libdir}/libmustach-core.so
%{pkgdir}/libmustach-core.pc
%{_includedir}/mustach/mustach.h
%{_includedir}/mustach/mustach-wrap.h
%dir %{_includedir}/mustach
%doc AUTHORS
%doc CHANGELOG.md
%doc README.md

#---------------------------------------------
%files lib-json-c
%{_libdir}/libmustach-json-c.so.%{so_version}{,.*}
%license LICENSE.txt

#---------------------------------------------
%files lib-json-c-devel
%{_libdir}/libmustach-json-c.so
%{pkgdir}/libmustach-json-c.pc
%{_includedir}/mustach/mustach-json-c.h

#---------------------------------------------
%files lib-cjson
%{_libdir}/libmustach-cjson.so.%{so_version}{,.*}
%license LICENSE.txt

#---------------------------------------------
%files lib-cjson-devel
%{_libdir}/libmustach-cjson.so
%{pkgdir}/libmustach-cjson.pc
%{_includedir}/mustach/mustach-cjson.h

#---------------------------------------------
%files lib-jansson
%{_libdir}/libmustach-jansson.so.%{so_version}{,.*}
%license LICENSE.txt

#---------------------------------------------
%files lib-jansson-devel
%{_libdir}/libmustach-jansson.so
%{pkgdir}/libmustach-jansson.pc
%{_includedir}/mustach/mustach-jansson.h

#---------------------------------------------
%changelog
* Wed Apr 26 2023 Marcus Hardt <marcus@hardt-it.de>
- initial packaging
