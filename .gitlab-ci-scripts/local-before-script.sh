#!/bin/bash

case ${RELEASE} in
    buster|bionic|focal)
        DEB_TARGET_MULTIARCH ?= $(shell dpkg-architecture -qDEB_TARGET_MULTIARCH).
        [ -e /usr/lib/${DEB_TARGET_MULTIARCH}/pkgconfig/libcjson.pc ] || {
            cat << EOF > /usr/lib/${DEB_TARGET_MULTIARCH}/pkgconfig/libcjson.pc
libdir=${prefix}/lib/x86_64-linux-gnu
includedir=${prefix}/cjson

Name: libcjson
Version: 1.7.10
Description: Ultralightweight JSON parser in ANSI C
URL: https://github.com/DaveGamble/cJSON
Libs: -L${libdir} -lcjson
Libs.private: -lm
Cflags: -I${includedir} -I${includedir}/cjson
EOF
        }
    ;;
esac
